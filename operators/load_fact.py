
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

class LoadFactOperator(BaseOperator):

    ui_color = '#F98866'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id="", 
                 table="",
                 sql_statement="",
                 truncate_data=False,
                 *args, **kwargs):

        super(LoadFactOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.table=table
        self.sql_statement=sql_statement
        self.truncate_data=truncate_data

    def execute(self, context):
        redshift = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        self.log.info('Starting to load fact table %s' % self.table_name)
        
        if self.truncate_data:
            truncate_sql = f'''
                TRUNCATE TABLE {self.table};
            '''
            redshift.run(truncate_sql)
            
        sql_statement = f'''
          INSERT INTO public.{self.table} 
          {self.sql_statement}
         '''
        redshift.run(sql_statement)