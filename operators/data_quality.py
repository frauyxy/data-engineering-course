from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

class DataQualityOperator(BaseOperator):

    ui_color = '#89DA59'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id="",
                 table="",
                 *args, **kwargs):

        super(DataQualityOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.table=talbe

    def execute(self, context):
        self.log.info('DataQualityOperator start now')
        redshift_hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)      
        for table in self.table:
            records = redshift_hook.get_records(f'select count(*) from {table}')
            if len(records) < 1 or len(records[0]) < 1:
                raise ValueError(f'''
                Data quality check failed
                ''')
            
        dq_checks=[
            {'table': 'public.artists',
            'check_sql': "SELECT COUNT(*) FROM public.artists WHERE artistid IS NULL",
            'expected_result': 0},
           	{'table': 'public.songs',
            'check_sql': "SELECT COUNT(*) FROM public.songs WHERE songid IS NULL",
            'expected_result': 0},
            {'table': 'public.time',
            'check_sql': "SELECT COUNT(*) FROM public.time WHERE start_time IS NULL",
            'expected_result': 0},
            {'table': 'public.users',
            'check_sql': "SELECT COUNT(*) FROM public.users WHERE userid IS NULL",
            'expected_result': 0}
        ]
            
        for check in dq_checks:
            records = redshift_hook.get_records(check['check_sql'])
            if len(records) != check['expected_result']:
                raise ValueError(f"Data quality check failed on {check['check_sql']}. Expected {check['expected_result']} but got {len(records)}")
            else:
              self.log.info("Data Quality Check Completed Succesfully")