# Project: Data Warehouse

## Introduction
A music streaming startup, Sparkify, has grown their user base and song database and want to move their processes and data onto the cloud. Their data resides in S3, in a directory of JSON logs on user activity on the app, as well as a directory with JSON metadata on the songs in their app.

The tasks of this project is building an ETL pipeline that extracts the data from S3, stages them in Redshift, and transforms data into a set of dimensional tables for Sparkfiy analytics team to continue finding insights in what songs Sparkfiy's users are listening to.

You'll be able to test your database and ETL pipeline by running queries given to you by the analytics team from Sparkify and compare your results with their expected results.

## Project Description

The database will be created with a star shema and load the temporary log data and song data to Redshift.

### Project files

The connection data to the data warehouse on AWS will be saved in **dwh.cfg**.

**sql_queries.py** contains the information: 
- how to drop the tables if they are existing.
- how to create the table with the data type.
- how to insert the data to each tables, including copy the data from S3. 

With **create_tables.py** you can create the data table in predefined Redshift. You will load the data with **etl.py** where the ETL process is descriped. 

### Data tables

There are totally 7 tables, 2 fact tables (staging_events, staging_songs) and 5 dimentional tables(songplay,users,artist,time, song). 

![database_sehma](shema.PNG)

## How to run the ETL pipeline

The following commands are to used for analytic teams to get the data. Please prepare the Redshift cluster, IAM and S3 before you run the code.

`!python create_tables.py` : create the data table in Redshift.

`!python etl.py` : copy and insert the data to each dimensional table.

Not forget to delete the cluster after loading the data!

## Example Queries

There are some example queries to show and hopefully can improve the understanding.

1. Create the table

```sql
DROP TABLE IF EXISTS tab1 
CREATE TABLE tab1 (
  variable_name variable_type [PRIMARY KEY] [NOT NULL]
  ... ...
)
```

2. Copy the data/ stage the data from S3

```
COPY {table_name} 
FROM {S3 database}
CREDENTIALS 'aws_iam_role={ARN}'
REGION 'us-west-2'
COMPUPDATE OFF
JSON AS'{auto or Jsonpath}'
```

The variables that are inside of {} can be replaced as required. 

3. Insert the data

```
INSERT INTO table2 (variable1, ...)
SELECT ...
FROM ...
[WHERE ...]
```
The insert stattement brings the data to the predefined tables.