import configparser


# CONFIG
config = configparser.ConfigParser()
config.read('dwh.cfg')

# DROP TABLES

staging_events_table_drop = "DROP TABLE IF EXISTS staging_events"
staging_songs_table_drop = "DROP TABLE IF EXISTS staging_songs"
songplay_table_drop = "DROP TABLE IF EXISTS songplay"
user_table_drop = "DROP TABLE IF EXISTS users"
song_table_drop = "DROP TABLE IF EXISTS song"
artist_table_drop = "DROP TABLE IF EXISTS artist"
time_table_drop = "DROP TABLE IF EXISTS time"

# CREATE TABLES

staging_events_table_create= ("""
CREATE TABLE staging_events (
  num_event INT IDENTITY(0,1) PRIMARY KEY NOT NULL,
  artist VARCHAR,
  auth VARCHAR,
  first_name VARCHAR,
  gender VARCHAR,
  item_in_session INT,
  last_name VARCHAR,
  length NUMERIC,
  level VARCHAR,
  location VARCHAR,
  method VARCHAR ,
  page VARCHAR,
  registration NUMERIC,
  session_id INT,
  song VARCHAR,
  status INT,
  ts NUMERIC,
  user_agent VARCHAR,
  user_id INT
);
""")

staging_songs_table_create = ("""
CREATE TABLE staging_songs (
  num_songs INT IDENTITY(0,1),
  artist_id VARCHAR,
  artist_latitude NUMERIC,
  artist_longitude NUMERIC,
  artist_location VARCHAR,
  artist_name VARCHAR,
  song_id VARCHAR PRIMARY KEY, 
  title VARCHAR,
  duration NUMERIC,
  year INT
);
""")

songplay_table_create = ("""
CREATE TABLE songplay (
  songplay_id INT IDENTITY(0,1) PRIMARY KEY NOT NULL,
  start_time timestamp,
  user_id INT,
  level VARCHAR,
  song_id VARCHAR,
  artist_id VARCHAR,
  session_id VARCHAR,
  artist_location VARCHAR,
  user_agent VARCHAR 
);
""")

user_table_create = ("""
CREATE TABLE users (
  user_id INT PRIMARY KEY NOT NULL,
  first_name VARCHAR,
  last_name VARCHAR,
  gender VARCHAR,
  level VARCHAR
);
""")

song_table_create = ("""
CREATE TABLE song (
  song_id VARCHAR PRIMARY KEY NOT NULL,
  song_title VARCHAR,
  artist_id VARCHAR NOT NULL,
  year INTEGER,
  duration NUMERIC
);
""")

artist_table_create = ("""
CREATE TABLE artist (
  artist_id VARCHAR PRIMARY KEY NOT NULL,
  artist_name VARCHAR,
  artist_location VARCHAR, 
  artist_latitude NUMERIC,
  artist_longitude NUMERIC
);
""")

time_table_create = ("""
CREATE TABLE time (
  start_time timestamp PRIMARY KEY NOT NULL,
  hour INT,
  day INT,
  week INT,
  month INT,
  year INT,
  weekday INT
);
""")

# STAGING TABLES

staging_events_copy = ("""
COPY staging_events 
FROM {}
CREDENTIALS 'aws_iam_role={}'
REGION 'us-west-2'
COMPUPDATE OFF
JSON AS {}
""").format(config.get('S3','LOG_DATA'), config.get('IAM_ROLE', 'ARN'), config.get('S3','LOG_JSONPATH'))

staging_songs_copy = ("""
COPY staging_songs 
FROM {}
CREDENTIALS 'aws_iam_role={}'
REGION 'us-west-2'
COMPUPDATE OFF
JSON AS'auto'
""").format(config.get('S3','SONG_DATA'), config.get('IAM_ROLE', 'ARN'))

# FINAL TABLES

songplay_table_insert = ("""
 INSERT INTO songplay (start_time, user_id, level, song_id, artist_id, session_id, artist_location, user_agent)
 SELECT TIMESTAMP 'epoch' + events.ts/1000*INTERVAL '1 second' AS start_time,
        events.user_id,
        events.level,
        songs.song_id,
        songs.artist_id,
        events.session_id,
        events.location,
        events.user_agent
FROM staging_events events, staging_songs songs
WHERE events.page = 'NextSong' 
  AND events.artist = songs.artist_name
  AND events.location = songs.artist_location
  AND events.song = songs.title
  AND events.length = songs.duration
""")

user_table_insert = ("""
 INSERT INTO users (user_id, first_name, last_name, gender, level)
SELECT DISTINCT  
    user_id, 
    first_name, 
    last_name, 
    gender, 
    level
FROM staging_events
WHERE page = 'NextSong'
""")

song_table_insert = ("""
 INSERT INTO song (song_id, song_title, artist_id, year, duration)
 SELECT DISTINCT 
   song_id,
   title,
   artist_id,
   year,
   duration
FROM staging_songs
WHERE song_id IS NOT NULL
""")

artist_table_insert = ("""
 INSERT INTO artist (artist_id, artist_name, artist_location, artist_latitude, artist_longitude)
SELECT DISTINCT
  artist_id,
  artist_name,
  artist_location,
  artist_latitude,
  artist_longitude
FROM staging_songs
WHERE artist_id IS NOT NULL
""")

time_table_insert = ("""
 INSERT INTO time (start_time, hour, day, week, month, year, weekday)
 SELECT DISTINCT 
   start_time,
   EXTRACT(hour from start_time),
   EXTRACT(day from start_time),
   EXTRACT(week from start_time),
   EXTRACT(month from start_time),
   EXTRACT(year from start_time),
   EXTRACT(dayofweek from start_time)
 FROM songplay
""")

# QUERY LISTS

create_table_queries = [staging_events_table_create, staging_songs_table_create, songplay_table_create, user_table_create, song_table_create, artist_table_create, time_table_create]
drop_table_queries = [staging_events_table_drop, staging_songs_table_drop, songplay_table_drop, user_table_drop, song_table_drop, artist_table_drop, time_table_drop]
copy_table_queries = [staging_events_copy, staging_songs_copy]
insert_table_queries = [songplay_table_insert, user_table_insert, song_table_insert, artist_table_insert, time_table_insert]
