# Data Engineering Course

This project is only for the udacity course - Data Engineering. All the things are done by myself. 

## Why you should check this project 

I'm taking this course from 09.2021 until 02.2022 the udacity couse and improve my data engineering skills.
You will find all the project work that I finished. This can show you my code skills, my practices and my understanding for the data engineering role.

Please take a look free and contant me if you are interested in my project.

## If you want to contact with me

You can search my name in the LinkedIn and [contact me](https://de.linkedin.com/in/xueying-yuan-9a0458137?trk=public_profile_browsemap) directly. 
My strength is analysing the data science problems, including BI, data analysis and data engineering. 
I can programm using SQL, R and Python. As you seen, I can also use spark and canssadra. 

