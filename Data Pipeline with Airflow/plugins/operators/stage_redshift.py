from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.utils.decorators import apply_defaults

class StageToRedshiftOperator(BaseOperator):
     """
        :Param redshift_conn_id: What Redshift Database are you connecting to.
        :Param aws_credentials_id: AWS User credentials you have saved in Airflow Connections.
        :Param table: Staging table name you are loading data into.
        :Param s3_bucket: Amazon S3 Bucket Name.
        :Param s3_key: Object Key Name in S3 Bucket.
        :Param json_path: How do you want your JSON to be copied.
    """
    ui_color = '#358140'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id="",
                 aws_credentials_id="",
                 table="",
                 s3_bucket="",
                 s3_key="",
                 json_path="auto",
                 *args, **kwargs):

        super(StageToRedshiftOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.aws_credentials_id=aws_credentials_id
        self.table=table
        self.s3_bucket=s3_bucket
        self.s3_key=s3_key
        self.json_path=json_path

    def execute(self, context):
        self.log.info("Build up the AWS connection")
        aws_hook = AwsHook(self.aws_credentials_id)
        credentials = aws_hook.get_credentials()
        redshift_hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        
        self.log.info("Clearing data from the desination Redshift table")
        redshift_hook.run(f'DELETE FROM {self.table}')
        
        self.log.info("Staging data from S3 to Redshift")
        rendered_key = self.s3_key.format(**context)
        s3_path = f's3://{self.s3_bucket}/{rendered_key}'
        staged_sql = f'''
            COPY {self.table}
            FROM '{self.s3_path}'
            ACCESS_KEY_ID '{credentials.access_key}'
            SECRET_ACCESS_KEY '{credentials.secret_key}'
            FORMAT AS JSON '{self.json_path}';
        '''
   
        redshift.run(staged_sql)
        self.log.info("Done with Redshit Stage Operation")