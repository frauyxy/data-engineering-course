##example dag
from datetime import datetime, timedelta
import os
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators import (StageToRedshiftOperator, LoadFactOperator,
                               LoadDimensionOperator, DataQualityOperator)

from helpers import SqlQueries
from airflow.operators.postgres_operator import PostgresOperator

AWS_KEY = os.environ.get('AWS_KEY')
AWS_SECRET = os.environ.get('AWS_SECRET')

default_args = {
    'owner': 'udacity',
    'start_date': datetime(2021, 12, 20),
    'end_date': datetime(2021, 12, 20),
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'email_on_retry': False,
    'depends_on_past': False,
    'catchup': False,
}

dag = DAG('udac_proj_dag',
          default_args=default_args,
          description='Load and transform data in Redshift with Airflow',
          schedule_interval='@hourly',
          max_active_runs=1
        )

start_operator = DummyOperator(task_id='Begin_execution', dag=dag)

stage_events_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    redshift_conn_id="redshift",
    aws_credentials_id="aws_credentials",
    talbe="staging_events",
    s3_bucket="udacity_dend",
    s3_key="log_data/2018/10/"
)

stage_songs_to_redshift = StageToRedshiftOperator(
    task_id='Stage_songs',
    dag=dag,
    redshift_conn_id='redshift',
    aws_credentials_id="aws_credentials",
    talbe="staging_songs",
    s3_bucket='udacity_dend',
    s3_key='song_data/A/A/A/'
)

load_songplays_table = LoadFactOperator(
    task_id='Load_songplays_fact_table',
    dag=dag,
    redshift_conn_id='redshift',
    table='songplay',
    table_columns=f'''
     playid, start_time, userid, level, songid, artistid, sessionid, location, user_agent
    ''',
    sql_statement=f'''
    SELECT md5(events.sessionid || events.start_time) songplay_id
        TIMESTAMP 'epoch' + events.ts/1000*INTERVAL '1 second' AS start_time,
        events.userid,
        events.level,
        songs.song_id,
        songs.artist_id,
        events.sessionid,
        events.location,
        events.useragent
    FROM staging_events events, staging_songs songs
    WHERE events.page = 'NextSong' 
      AND events.artist = songs.artist_name
      AND events.location = songs.artist_location
      AND events.song = songs.title
      AND events.length = songs.duration
    '''
)

load_user_dimension_table = LoadDimensionOperator(
    task_id='Load_user_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    table='users',
    sql_statement=f'''
      SELECT DISTINCT  
        userid, 
        firstname, 
        lastname, 
        gender, 
        level
    FROM public.staging_events
    WHERE page = 'NextSong'
    '''
)

load_song_dimension_table = LoadDimensionOperator(
    task_id='Load_song_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    table='songs',
    sql_statement=f'''
      SELECT DISTINCT song_id,
        title,
        artist_id,
        year,
        duration
      FROM public.staging_songs
      WHERE song_id IS NOT NULL
    '''
)

load_artist_dimension_table = LoadDimensionOperator(
    task_id='Load_artist_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    table='artists',
    sql_statement=f'''
       SELECT DISTINCT artist_id,
        artist_name,
        artist_location,
        artist_latitude,
        artist_longitude
    FROM  public.staging_songs
    WHERE artist_id IS NOT NULL
    '''
)

load_time_dimension_table = LoadDimensionOperator(
    task_id='Load_time_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    table=f'time',
    sql_statement=f'''
     SELECT DISTINCT  start_time,
        EXTRACT(hour from start_time),
        EXTRACT(day from start_time),
        EXTRACT(week from start_time),
        EXTRACT(month from start_time),
        EXTRACT(year from start_time),
        EXTRACT(dayofweek from start_time)
     FROM public.songplays
    '''
)

run_quality_checks = DataQualityOperator(
    task_id='Run_data_quality_checks',
    dag=dag,
    redshift_conn_id="redshift",
    table=(songplays,songs,users,artists,time)
)

end_operator = DummyOperator(task_id='Stop_execution',  dag=dag)

start_operator >> stage_events_to_redshift
start_operator >> stage_songs_to_redshift
stage_events_to_redshift >> load_songplays_table
stage_songs_to_redshift >> load_songplays_table
load_songplays_table >> load_user_dimension_table
load_songplays_table >> load_song_dimension_table
load_songplays_table >> load_time_dimension_table
load_songplays_table >> load_time_dimension_table
load_user_dimension_table >> run_quality_checks
load_song_dimension_table >> run_quality_checks
load_artist_dimension_table >> run_quality_checks
load_time_dimension_table >> run_quality_checks
run_quality_checks >> end_operator
