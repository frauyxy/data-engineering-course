# Projekt: Data Modelling with Postgres
=============================================

## Introduction
------------------------------------------------

A startup called Sparkify wants to analyze the data they've been collecting on songs and user activity on their new music streaming app. The analytics team is particularly interested in understanding what songs users are listening to. Currently, they don't have an easy way to query their data, which resides in a directory of JSON logs on user activity on the app, as well as a directory with JSON metadata on the songs in their app.

In this project, we create a Postgres database with tables designed to optimize queries on song play analysis.


## Project Description
----------------------------------------------------------

In this project, you'll apply what you've learned on data modeling with Postgres and build an ETL pipeline using Python. To complete the project, you will need to define fact and dimension tables for a star schema for a particular analytic focus, and write an ETL pipeline that transfers data from files in two local directories into these tables in Postgres using Python and SQL. 

## Data Modeling
-----------------------------------------------------------

There are total 5 tables, namly: songplays, users, artists, and time. They are divided into 5 tables:

**Fact Table**
	1. songplays - records in log data associated with song plays i.e. records with page NextSong include:
    songplay_id, start_time, user_id, level, song_id, artist_id, session_id, location, user_agent
**Dimension Tables**
	1. users - users in the app include:
    user_id, first_name, last_name, gender, level
	2. songs - songs in music database include
    song_id, title, artist_id, year, duration
	3. artists - artists in music database include
    artist_id, name, location, latitude, longitude
	4. time - timestamps of records in songplays broken down into specific units
		○ start_time, hour, day, week, month, year, weekday


## ETL Process
-------------------------------------
**Process Song data**

Song data is from the JSON file song_data and provides the information for table songs and artists.

**Process Log data**
The collection of data is from the JSON file log_data and provides the information for table time, users and songplays.


## Explanition of files 
-------------------------------------

A fact table, songplays, has a star schema and its dimensions are songs, users, artist and time. 

sql_queries.py: concludes all the SQL queries, including create the tables, insert the value, a query for find songs and a query list. 

create_tables.py: set up the connection to the database and creat the tables if they don't exist after connected with the database. All the SQL queries are based on sql_queries.py. 

etl.ipynb: process the whole analysis with a part data and begins with loading song data to exact different data for the different tables. And process the log data and finish the analysis. It builds a whole ETL process. 

etl.py: the same logic as etl.ipynb and is a script without any testing data. 

test.ipynb: contains the sQL-Queries to check the table content whether it's correct. 



## how to run the Python scripts
-------------------------------------
 First, make sure you've installed all the relevant packages. 
 
 Build the tables with `%run create_tables.py`. 
 
 Check the data with `%run test.ipynb`
 
 Using etl.py to run the ETL process.
 
 Finanly you can chek with test.ipynb.
 
 
 
 